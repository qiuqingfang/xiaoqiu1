package demo1;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.File;
import java.net.URL;

public class TestJsoup {

    @Test
    public void testJsoupUrl() throws Exception {
        //    解析url地址
        Document document = Jsoup.parse(new URL("https://www.jd.com/"), 1000);

        //获取title的内容
        Element title = document.getElementsByTag("title").first();
     //   System.out.println(title.text());


    }

    @Test
    public void test2()throws Exception{
        String html = FileUtils.readFileToString(new File("E:/abc.html"),"utf-8");
        Document document =Jsoup.parse(html);
        Element element = document.getElementById("search_title");
        System.out.println(element);
    }

    @Test
    public void test3()throws Exception{
        //    解析url地址
        Document document = Jsoup.parse(new URL("https://www.jd.com/"), 1000);

        Elements elements = document.select("span");
        for (Element element : elements) {
            System.out.println(element.text());
        }

        Element select = document.select("#baidu").first();
        System.out.println("aaaa"+select);
    }


}
