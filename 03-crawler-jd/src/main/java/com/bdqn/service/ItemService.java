package com.bdqn.service;

import com.bdqn.entity.Item;

import java.util.List;

public interface ItemService {
    void save(Item item)throws Exception; // 保存

    List<Item> findAll(Item item)throws Exception;
}
