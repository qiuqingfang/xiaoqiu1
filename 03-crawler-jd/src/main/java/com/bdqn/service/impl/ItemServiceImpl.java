package com.bdqn.service.impl;

import com.bdqn.entity.Item;
import com.bdqn.repository.ItemRepository;
import com.bdqn.service.ItemService;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.beans.Transient;
import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {
    @Resource
    private ItemRepository itemRepository;
    @Override
    public void save(Item item) throws Exception {
        itemRepository.save(item);
    }

    @Override
    public List<Item> findAll(Item item) throws Exception {
        Example<Item> example = Example.of(item);
        return itemRepository.findAll(example);
    }
}
