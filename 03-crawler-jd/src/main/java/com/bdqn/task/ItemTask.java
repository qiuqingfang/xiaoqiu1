package com.bdqn.task;

import com.bdqn.entity.Item;
import com.bdqn.service.ItemService;
import com.bdqn.utils.HttpUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

@Component
public class ItemTask {
    @Resource
    private HttpUtils httpUtils;

    @Resource
    private ItemService itemService;
    @Scheduled(cron = "0 21 22 * * ?")
    public void ItemTask () throws Exception {
        String keyword="手机";
        String url="https://search.jd.com/Search?keyword="+keyword+"&page=";
    for (int i=1;i<5;i=i+2){
        this.parse(url+i);
    }

    }

    private void parse(String url) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        //解析京东的html
        Document document = Jsoup.parse(new URL(URLDecoder.decode(url,"UTF-8")),1000*60*5);
        Element element = document.getElementById("J_goodsList");//获取div
        Elements elements = element.select("li.gl-item");//获取商品所在的li标签
        //循环
        for (Element spuEle : elements) {
            //获取data-spu
            String spuValue = spuEle.attr("data-spu");
            //判断当前标签是否有data-spu元素并且不为空
            if(spuEle.hasAttr("data-spu") && StringUtils.isNotBlank(spuValue)){
                System.out.println(spuEle.attr("data-spu"));
                //获取商品sku数据
                Elements skus = spuEle.select("li.ps-item img");
                for (Element skuEle : skus) {
                    String skuId = skuEle.attr("data-sku");//sku

                    //判断商品是否被抓取过，可以根据sku判断
                    Item param = new Item();
                    param.setSku(Long.parseLong(skuId));
                    List<Item> list = this.itemService.findAll(param);
                    //判断是否查询到结果
                    if (list.size() > 0) {
                        //如果有结果，表示商品已下载，进行下一次遍历
                        continue;
                    }

                    //商品详情地址
                    String detailUrl = "https://item.jd.com/" + skuId + ".html";
                    //解析京东的商品详情页面
                    Document detailDoc = Jsoup.parse(new URL(URLDecoder.decode(detailUrl,"UTF-8")),30000);
                    //获取商品标题
                    String title = detailDoc.select("div.sku-name").text();
                    //获取商品价格
                    String priceUrl = "https://p.3.cn/prices/mgets?skuIds=J_"+skuId;
                    //解析商品价格JSON串
                    String priceJson = httpUtils.doGetHtml(priceUrl);
                    //解析json数据获取商品价格
                    double price = objectMapper.readTree(priceJson).get(0).get("p").asDouble();
                    //获取图片地址
                    String pic = "https:" + skuEle.attr("data-lazy-img").replace("/n7/","/n1/");
                    //下载图片
                    String picName = this.httpUtils.doGetImage(pic);

                    //封装商品数据
                    Item item = new Item();
                    item.setSku(Long.parseLong(skuId));
                    item.setTitle(title);
                    item.setPrice(price);
                    item.setCreated(new Date());
                    item.setUpdated(item.getCreated());
                    item.setUrl(detailUrl);
                    item.setSpu(Long.parseLong(spuValue));
                    item.setPic(picName);
                    //保存到数据库
                    this.itemService.save(item);
                }

            }

        }
    }
}
