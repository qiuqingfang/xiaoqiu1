package com.bdqn.demo3;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class PoolDemo {

    public static void main(String[] args) {
        PoolingHttpClientConnectionManager cm=
        new PoolingHttpClientConnectionManager() ;
        cm.setMaxTotal(100);
        cm.setDefaultMaxPerRoute(10);

        doGet(cm);
        doGet(cm);


    }

    public static  void doGet(PoolingHttpClientConnectionManager cm){
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;

        try {

            httpClient = HttpClients.custom().setConnectionManager(cm).build();

            HttpGet httpGet = new HttpGet("https://www.jd.com/");


            //配置请求信息
            RequestConfig config = RequestConfig.custom().setConnectTimeout(100000)   //创建连接的最长时间，单位是毫秒
                    .setConnectionRequestTimeout(5000)   //设置获取连接的最长时间，单位是毫秒
                    .setSocketTimeout(15*1000)      //设置数据传输的最长时间，单位是毫秒
                    .build();

            //给请求设置请求信息
            httpGet.setConfig(config);

            response = httpClient.execute(httpGet);

            if(response.getStatusLine().getStatusCode()==200){

                HttpEntity entity =response.getEntity();
                String content = EntityUtils.toString(entity, "utf8");
                System.out.println(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
