package com.bdqn.demo2;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpPostParamDemo {

    public static void main(String[] args) {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        try {
            httpClient= HttpClients.createDefault();

            URIBuilder uriBuilder =new URIBuilder("https://list.tmall.com/search_product.htm");
            uriBuilder.setParameter("q","手机");

            HttpPost httpPost =new HttpPost(uriBuilder.build());

            response = httpClient.execute(httpPost);

            if(response.getStatusLine().getStatusCode()==200){
                HttpEntity entity =response.getEntity();
                String content = EntityUtils.toString(entity, "utf8");
                System.out.println(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                if(httpClient!=null){
                    httpClient.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
