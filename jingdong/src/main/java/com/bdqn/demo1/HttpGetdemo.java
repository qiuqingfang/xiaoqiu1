package com.bdqn.demo1;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class HttpGetdemo {
    public static void main(String[] args) {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;

        try {

            httpClient = HttpClients.createDefault();

            HttpGet httpGet = new HttpGet("https://www.jd.com/");

            response = httpClient.execute(httpGet);

            if(response.getStatusLine().getStatusCode()==200){

                HttpEntity entity =response.getEntity();
                String content = EntityUtils.toString(entity, "utf8");
            System.out.println(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                if(httpClient!=null){
                    httpClient.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
