package com.bdqn.demo1;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

public class HttpGetParamDemo {
    public static void main(String[] args) {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;

        try {
            httpClient = HttpClients.createDefault();
            URIBuilder builder =new URIBuilder("https://www.jd.com/");

        builder.setParameter("keyword","java");
            HttpGet httpGet = new HttpGet(builder.build());
            response = httpClient.execute(httpGet);

            if(response.getStatusLine().getStatusCode()==200){
                HttpEntity entity =response.getEntity();
                String content= EntityUtils.toString(entity,"utf8");
           System.out.println(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                if(httpClient!=null){
                    httpClient.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
